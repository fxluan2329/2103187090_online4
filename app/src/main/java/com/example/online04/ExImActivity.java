package com.example.online04;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ExImActivity extends AppCompatActivity {

    private Button explisit;
    private Button implisit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exim);

        explisit = (Button) findViewById(R.id.explisit);
        explisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openEx();
            }
        });

        implisit = (Button) findViewById(R.id.implisit);
        implisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openIm();
            }
        });
    }

    public void openEx(){
        Intent intent = new Intent(this, ExplisitActivity.class);
        startActivity(intent);
    }

    public void openIm(){
        Intent intent = new Intent("com.example.pemroglanjut.CaltActivity");
        startActivity(intent);
    }
}
